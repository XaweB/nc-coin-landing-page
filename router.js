const express = require('express');
const router = express.Router();

const viewsController = require('./controllers/viewsController');

router.get('/', viewsController.renderPage_home);

router.get('/howitworks', viewsController.renderPage_howItWorks);

router.get('/howtobuy', viewsController.renderPage_howToBuy);
module.exports = router;