class ExplanationDisplayer {
    constructor() {
        this.exps = document.querySelectorAll('.explanation');
        this.expButtons = document.querySelectorAll('.button-explanation');

        this.hideExp = (exp, btn, time = 200) => {
            let btnTextTag = btn.querySelector('.button-text');
            if(!btnTextTag)
                btnTextTag = btn;

            console.log(btnTextTag)

            btnTextTag.textContent = "I know already!";
            $(exp).slideUp(time);
        }
        this.showExp = (exp, btn, time = 200) => {
            let btnTextTag = btn.querySelector('.button-text');
            if(!btnTextTag)
                btnTextTag = btn;

            btnTextTag.textContent = "Show me how!";
            $(exp).slideDown(time);
        }

        this.addListeners = () => {
            for(let i = 0; i < this.expButtons.length; i++) {
                let exp = this.exps[i];
                let btn = this.expButtons[i];

                console.log(exp)

                btn.addEventListener('click', () => {
                    
                    if(window.getComputedStyle(exp).display == 'none') {
                        this.showExp(exp, btn, 1000);
                    }
                    else {
                        this.hideExp(exp, btn, 1000);
                    }
                })
            }
        }
        this.addListeners()
    }
}
const explanationDisplayer = new ExplanationDisplayer();
